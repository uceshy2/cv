import numpy as np
_seed = 123
np.random.seed(_seed)

import tensorflow as tf
tf.random.set_seed(_seed)
from tensorflow.keras.layers import LSTM, Dense, Flatten, BatchNormalization
from tensorflow.keras.models import Sequential
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import optimizers

import pandas as pd

class Model:
    def __init__(self):
        self.data = None
        self.model = None
        
    def __build_model(self, input_shape, outputs, learning_rate, seed):
        '''
        Builds and returns the Deep Neural Network that will compute the allocation ratios
        that optimize the Sharpe Ratio of the portfolio
        
        inputs: 
                input_shape - tuple of the input shape 
                outputs - the number of assets
                learning_rate - learning rate of the optimizer
                seed - seed for weight initialization
                
        returns: an LSTM 
        '''
        model = Sequential([
            LSTM(64, input_shape=input_shape, kernel_initializer=tf.keras.initializers.GlorotUniform(seed=seed)),
            Flatten(),
            Dense(outputs, activation='softmax', kernel_initializer=tf.keras.initializers.GlorotUniform(seed=seed))
        ])

        def sharpe_loss(y_pred, data_values):
            # make all time-series start at 1
            _data_values = tf.divide(data_values, data_values[0])  
            
            return_t = _data_values[1:]  / _data_values[:-1] - 1
            portfolio_returns = tf.reduce_sum(tf.multiply(return_t, y_pred[:-1]), axis=1)
                        
            sharpe = K.mean(portfolio_returns) / K.std(portfolio_returns)
            
            # since we want to maximize Sharpe, while gradient descent minimizes the loss, 
            #   we can negate Sharpe (the min of a negated function is its max)
            return -sharpe
        
        model.compile(loss=sharpe_loss, optimizer=optimizers.Adam(learning_rate=learning_rate))
        return model
    
    def train_model(self, data: pd.DataFrame, lags=50, learning_rate=0.001, epochs=100, batch_size=64, validation_split=0.1, verbose=1, seed=3):
        '''
        Computes and returns the allocation ratios that optimize the Sharpe over the given data
        
        input: 
            data - DataFrame of historical closing prices of various assets
            lags - number of lags on the input data
            learning_rate - learning rate of the optimizer
            epochs - number of epochs
            batch_size - size of the mini-batch
            validation_split - percentage of the training sample used for validation (if>0, then early stopping is activated)
            verbose - whether to show training progresses
            seed - seed for weight initialization
        
        return: trained model
        '''
        
        self.lags = lags
        
        data_w_ret = self.build_inputs_data(data)
        
        data = data.iloc[lags+1:]
        self.data = tf.cast(tf.constant(data), float)
        
        fit_predict_data = np.reshape(data_w_ret, (data_w_ret.shape[0], 1, data_w_ret.shape[1]))
        
        if self.model is None:
            self.model = self.__build_model(fit_predict_data.shape[1:], len(data.columns), learning_rate=learning_rate, seed=seed)
        
        # if validation is higher then zero then activate early stopping
        if validation_split>0:
            es = EarlyStopping(monitor='val_loss', mode='min', verbose=verbose, patience=5, restore_best_weights=True)
            self.model.fit(fit_predict_data, data.values, epochs=epochs, shuffle=False, batch_size=batch_size, validation_split=validation_split, callbacks=[es], verbose=verbose)
        else:
            self.model.fit(fit_predict_data, data.values, epochs=epochs, shuffle=False, batch_size=batch_size, verbose=verbose)
        return self
    
    def predict_allocation(self, data: pd.DataFrame):
        '''
        Computes and returns the allocation ratios given the trained model
        
        input: data - DataFrame of historical closing prices of various assets
        
        return: optimal allocation
        '''
        data_w_ret = self.build_inputs_data(data, fit_transorm=False)
        fit_predict_data = np.reshape(data_w_ret, (data_w_ret.shape[0], 1, data_w_ret.shape[1]))
        return self.model.predict(fit_predict_data)

        
    def build_inputs_data(self, data, fit_transorm=True):
        '''
        Computes and returns the input data

        input: 
            data - DataFrame of historical closing prices of various assets
            fit_transorm - whether to fit the transformation parameters

        return: input features transformed (scaled)
        '''
        # data with returns
        data_w_ret = np.concatenate([ data.values[1:], data.pct_change().values[1:] ], axis=1)
        # and lags
        if self.lags > 0:
            data_w_ret_lags = data_w_ret.copy()
            data_w_ret = pd.DataFrame(data_w_ret)
            for ll in range(self.lags):
                data_w_ret_lags = np.concatenate([data_w_ret_lags[1:], data_w_ret.shift(ll+1).values[ll+1:] ], axis=1)
            # replace
            data_w_ret = data_w_ret_lags.copy()
        # fit scaling transformation
        if fit_transorm:
            self.mu_x = np.mean(data_w_ret, axis=0)
            self.sigma_x = np.std(data_w_ret, axis=0)
        
        return (data_w_ret - self.mu_x) / self.sigma_x